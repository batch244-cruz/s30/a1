


//Activity
db.fruits.aggregate([
	{$match: {onSale: true}},
	{$count: "fruitsOnSale"}
]);

db.fruits.aggregate([
	{$match: {stock: {$gte: 20}}},
	{$count: "enoughStock"}
]);


db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", avg_price: {$avg: "$price"}}},
	{$sort: {total: -1}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", max_price: {$max: "$price"}}},
	{$sort: {total: -1}}
]);

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplierId", max_price: {$min: "$price"}}},
	{$sort: {total: 1}}
]);







